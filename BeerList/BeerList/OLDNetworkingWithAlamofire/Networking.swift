//
//  Networking.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 13/09/2019.
//  Copyright © 2019 Alvaro IDJOUBAR. All rights reserved.
//
/*
import Foundation
import Alamofire
import RealmSwift

enum ErrorNetwork: Error {
    case requestError
    case noData
    case noDataToSave
    case dataBaseFail
    case failedToSave
    
    var description: String {
        switch self {
        case .requestError:
            return "Internet conection error."
        case .noData:
            return "Response returned with no data to decode."
        case .noDataToSave:
            return "No data to save into database."
        case .dataBaseFail:
            return "Fail to create database."
        case .failedToSave:
            return "Fail to save into database."
        }
    }
}

final class Networking {
    static func request<T: Decodable>(ofType: T.Type, completion: @escaping (Swift.Result<T, ErrorNetwork>) -> ()) {
        Alamofire.request("https://api.punkapi.com/v2/beers",
                          method: Alamofire.HTTPMethod.get,
                          parameters: ["malt": "extra_pale"],
                          encoding: URLEncoding.default).responseJSON { response in
            switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let result = try JSONDecoder().decode(ofType.self, from: data)
                            completion(.success(result))
                        }
                        catch {
                            completion(.failure(ErrorNetwork.noData))
                        }
                }
            case .failure(_):
                    completion(.failure(ErrorNetwork.requestError))
            }
        }
    }
}
*/
