//
//  WeatherNetworkManager.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 14/02/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

struct PunkNetworkManager: NetworkManager {
    
    let router = Router<PunkApi>()

    func getModel<T : Decodable>(ofType: T.Type, completion: @escaping (Result<T, NetworkErrorResponse>) -> ()) {
        
        router.request(.beers) { result in
            switch result {
            case .failure(_):
                completion(.failure(NetworkErrorResponse.failed))
            case .success(let data):
                do {
                    let _ = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    let apiResponse = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(apiResponse))
                } catch (let error) {
                    print(error)
                    completion(.failure(NetworkErrorResponse.badRequest))
                }
            }
        }
    }
}

// HANDLE ERRORS WITH CODES - 404, 500, etc .....
