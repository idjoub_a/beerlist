//
//  DatabaseManager.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 15/09/2019.
//  Copyright © 2019 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit
import RealmSwift

enum ErrorDBManager: Error {
    case fetchDataError
    case editDataError
    case deleteDataError
    case unknown
    
    var description: String {
        switch self {
        case .fetchDataError:
            return "Failed to fetch data from local DB."
        case .editDataError:
            return "Failed to add data into local DB."
        case .deleteDataError:
            return "Failed to delete data into local DB."
        case .unknown:
            return "Unknown error from DB Manager."
        }
    }
}

class DBManager {
    
    private var database: Realm
    static let shared = DBManager()
    
    private init() {
        database = try! Realm()
    }
    
    func fetchDataFromDB<T: Object>(ofType: T.Type) -> Results<T> {
        return database.objects(T.self)
    }

    func addItem<T: Object>(object: T) throws {
        do {
            try database.write({ database.add(object) })
        } catch {
            throw ErrorDBManager.editDataError
        }
    }
    
    func deleteAllFromDatabase() throws {
        do {
            try database.write({ database.deleteAll() })
        } catch {
            throw ErrorDBManager.deleteDataError
        }
    }
}
