//
//  WeatherEndPoint.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 14/02/2020.
//  Copyright © 2020 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

public enum PunkApi {
    case beers
}

// Postman TEST URL : https://api.punkapi.com/v2/beers?malt=extra_pale

extension PunkApi: EndPointType {

    var environmentBaseURL: String {
        switch self {
        case .beers:
            return "https://api.punkapi.com"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        switch self {
        case .beers:
            return "/v2/beers"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .beers:
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["malt":"extra_pale"])
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}

