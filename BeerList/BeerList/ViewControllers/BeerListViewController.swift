//
//  BeerListViewController.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 13/09/2019.
//  Copyright © 2019 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit
import Kingfisher
import RealmSwift

typealias Beers = [Beer]

final class BeerListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let cellHeight: CGFloat = 500.0
    private var nameFilter: String = ""
    private let dataBaseManager: DBManager = DBManager.shared
    private let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
    
    private var fitleredBeerDatas: Results<Beer> {
        if let noDataViewXIB = Bundle.main.loadNibNamed("NoDataView", owner: self, options: nil)?.first as? NoDataView {
            tableView.backgroundView = beerDatas.count == 0 ? noDataViewXIB : nil
            tableView.isScrollEnabled = beerDatas.count == 0 ? false : true
        }

        let predicate = self.navigationItem.searchController?.searchBar.selectedScopeButtonIndex == 1 ? "tagline CONTAINS[c] '\(nameFilter)'" : "name CONTAINS[c] '\(nameFilter)'"
        return nameFilter == "" ? beerDatas : beerDatas.filter(predicate)
    }
    
    private var beerDatas: Results<Beer> {
        return self.dataBaseManager.fetchDataFromDB(ofType: Beer.self)
    }
    
    private let networkManager = PunkNetworkManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
             overrideUserInterfaceStyle = .light
         }
        
        setupNavbar()
        setupSearchBar()
        loadBeers()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.allowsSelection = false
        tableView.register(UINib.init(nibName: DetailsTableViewCell.nibName, bundle: nil),
                           forCellReuseIdentifier: DetailsTableViewCell.cellIdentifier)
        
        self.extendedLayoutIncludesOpaqueBars = true
        print("RealmFile location: \(String(describing: Realm.Configuration.defaultConfiguration.fileURL))")
    }

    private func setupNavbar(){
        self.title = "Punk Beers 🍺"
        self.navigationController?.navigationBar.prefersLargeTitles = true

        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = UIColor.init(red: 231.0/255, green: 76.0/255, blue: 60.0/255, alpha: 1.0)
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        else {
            self.navigationController?.navigationBar.tintColor = .white
            self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 231.0/255, green: 76.0/255, blue: 60.0/255, alpha: 1.0)
        }
        let activity = UIBarButtonItem(customView: self.activityIndicator)
        /* --- TEST BUTTONS --- */
        let rewind = UIBarButtonItem(barButtonSystemItem: .rewind, target: self, action: #selector(cleanRealmDB))
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(launchRequest))
        /*---               ---*/
        navigationItem.rightBarButtonItems = [rewind, refresh, activity]
        navigationItem.rightBarButtonItems?.forEach({$0.tintColor = .white})
    }

    @objc func cleanRealmDB() {
        do {
            try self.dataBaseManager.deleteAllFromDatabase()
        } catch {
            self.buildAlert(ErrorDBManager.deleteDataError.description)
        }
        self.tableView.reloadData()
    }
    
    @objc func launchRequest() {
        loadBeers()
    }
    
    private func setupSearchBar() {
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.searchTextField.backgroundColor = .white
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barTintColor = .white
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search by name"
        searchController.searchBar.scopeButtonTitles = ["Name", "Tagline"]
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        definesPresentationContext = true
        
        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            if let backgroundview = textfield.subviews.first {
                backgroundview.backgroundColor = .white
                backgroundview.layer.cornerRadius = 10;
                backgroundview.clipsToBounds = true;
            }
        }
    }

    private func buildAlert(_ errorMessage: String) {
        let alert = UIAlertController(title: errorMessage, message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Retry", style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func loadBeers() {
        self.activityIndicator.startAnimating()
        
        networkManager.getModel(ofType: Beers.self) { [unowned self] result in
            DispatchQueue.main.async {
                switch result {
                case .failure(let error):
                    self.buildAlert(error.description)
                case .success(let beers):
                    if beers.count <= 0 {
                        self.buildAlert(NetworkErrorResponse.noData.description)
                    } else {
                        do {
                            try self.dataBaseManager.deleteAllFromDatabase()
                            try beers.forEach({ try self.dataBaseManager.addItem(object: $0) })
                        } catch ErrorDBManager.deleteDataError {
                            self.buildAlert(ErrorDBManager.deleteDataError.description)
                        } catch ErrorDBManager.editDataError {
                            self.buildAlert(ErrorDBManager.editDataError.description)
                        } catch {
                            self.buildAlert(ErrorDBManager.unknown.description)
                        }
                    }
                }
                self.activityIndicator.stopAnimating()
                self.tableView.reloadData()
            }
        }
    }
}

extension BeerListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fitleredBeerDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: DetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: DetailsTableViewCell.cellIdentifier) as? DetailsTableViewCell
            else { return UITableViewCell() }

        cell.nameLabel.text = fitleredBeerDatas[indexPath.row].name
        cell.taglineLabel.text = fitleredBeerDatas[indexPath.row].tagline
        cell.descriptionLabel.text = fitleredBeerDatas[indexPath.row].beerDescription
        cell.picture.kf.setImage(with: URL(string: fitleredBeerDatas[indexPath.row].imageURL),
                                 placeholder: UIImage(named: "noPicture"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}

extension BeerListViewController: UISearchBarDelegate {
    
    private func reloadBeers(with filter: String) {
        nameFilter = filter
        tableView.reloadData()
    }
    
//    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        //reloadBeers(with: searchBar.text ?? "")
//    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        reloadBeers(with: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        reloadBeers(with: searchBar.text ?? "")
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        reloadBeers(with: "")
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        tableView.reloadData()
    }
}
