//
//  BeerList.swift
//  BeerList
//
//  Created by Alvaro IDJOUBAR on 13/09/2019.
//  Copyright © 2019 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

// MARK: - BeerElement
class Beer: Object, Codable {
    @objc dynamic var id: Int
    @objc dynamic var name, tagline, beerDescription: String
    @objc dynamic var imageURL: String
    
    enum CodingKeys: String, CodingKey {
        case id, name, tagline
        case beerDescription = "description"
        case imageURL = "image_url"
    }
}
